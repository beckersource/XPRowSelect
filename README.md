# XPRowSelect
## Windows XP Explorer Full Row Select, CTRL+N, & Network Connectivity Check

This program does the following:
* Hooks into Windows Explorer to allow for full-row selection of Explorer items while in Details/Report view-mode. Click anywhere on the item row to select.
* CTRL+N when an explorer window is focused will open a new explorer window at that same location.  CTRL+N on invalid paths (Desktop, My Commputer, etc) will go to My Computer.
* Performs a network check every 5 seconds and will run an 'offline' batch file one time when offline is detected. An online check is repeated every 60 seconds.  When connectivity is found, an 'on' batch file is run.
* Network offline and online changes (batch file executions) are logged to a log file in the same directory as the program.
* Right-click tray icon menu to open the network log file.

Command-line Args:
* '/H' to hide the tray icon.
* '/O' to disable network activity checks.

Other Stuff:
* Developed under Windows XP 32-bit Service Pack 3.  Untested on anything else.
* Win32 API only (no .NET).
* Visual Studio 6 / VC98 project included.  All code in a single CPP file.
* Runs in the background with an optional visible tray icon. Right-click tray icon to exit.
* Upon program launch, existing Explorer windows using Details/Report view will be adjusted for row selection.
* Mouse-hover on tray icon to view version/info.
* See main .cpp file for release notes.
  
___NOTE1: Full Row Select: Explorer windows _MUST_ be set to use "Details" viewing mode in order for full rows to be selected___ \
___NOTE2: CTRL+N: Explorer Folder View option "Full Path in Title Bar" must be enabled for CTRL+N feature to work___
